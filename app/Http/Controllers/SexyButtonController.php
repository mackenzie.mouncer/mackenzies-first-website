<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\SexyButton;

class SexyButtonController extends Controller
{
    function showButtonPage () {

        $sexybuttons = SexyButton::all();

        $data['sexybuttons'] = $sexybuttons;

        return view('alex-buttons')->with($data);

    }
}
